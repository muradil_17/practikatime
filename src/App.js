import React, {Component} from 'react';
import {Route, Switch, BrowserRouter} from "react-router-dom";
import Main from "./pages/main";
import Counter from "./components/Counter/Counter";
import Header from "./components/Header/Header";

class App extends Component {

    render() {
        return (
            <div>
                <BrowserRouter>
                    <Header/>
                    <Switch>
                        <Route path='/redux' component={Counter}/>
                        <Route path='/' component={Main}/>
                    </Switch>
                </BrowserRouter>
            </div>
        );
    }
}


export default App;
