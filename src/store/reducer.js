const  initialState = {
  value: 'Hello world',
  counter: 0
};

const reducer = (state = initialState, action) =>{
    switch (action.type) {
        case "CHANGE_VALUE":
            return {
                value: action.text
            };
        case "INCREMENT":
            return {
                ...state,
                counter: state.counter + 1
            };
        case "DECREMENT":
            return {
                ...state,
                counter: state.counter - 1
            };
        case "ADD_COUNTER":
            return {
                ...state,
                counter: state.counter + action.value
            };
        case "SUBTRACT_COUNTER":
            return {
                ...state,
                counter: state.counter - action.value
            };
        default:
            return state;
    }
};

export default reducer;