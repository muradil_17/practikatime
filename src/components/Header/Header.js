import React from 'react';
import {NavLink} from "react-router-dom";
import './Header.css';

function Header(props) {
    return (
        <div className='header'>
            <h1>Practika Time</h1>
            <NavLink to='/'>Home</NavLink>
            <NavLink to='/redux'>Redux</NavLink>
        </div>
    );
}

export default Header;