import React, {Component} from 'react';
import {connect} from 'react-redux'

class Main extends Component {
    render() {
        return (
            <div>
                <input onChange={this.props.changeValue} placeholder={this.props.value}/>
                <p>{this.props.value}</p>
            </div>
        );
    }
}

const mapStateProps = state =>{
  return{
      value: state.value
  }
};

const mapDispatchToProps = dispatch =>{
    return{
        changeValue: (event)=> {
            console.log(event.target.value);
            const action = {type: "CHANGE_VALUE", text: event.target.value};
            dispatch(action)
        }
    }
};

export default connect(mapStateProps, mapDispatchToProps)(Main);